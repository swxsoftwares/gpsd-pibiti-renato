#!/usr/bin/pytho
# -*- coding: utf-8 -*-

__author__ = "Renato Silveira"
__date__ = "$11/06/2014 10:00:00$"

ACCESS_KEY = "AKIAIAGU43PWYY7VQV4Q"
SECRET_ACCESS_KEY = "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"


"""
  - Voce deve projetar sua aplicacao para ser ideponente (nao executar a mesma requisicao duas vezes), pois existe uma possibilidade de que haja um erro em um dos servidores redundantes na nuvem da Amazon fazendo com que uma mensagem volte para a fila novamente. (http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/DistributedQueues.html)
"""


import boto.sqs
from boto.sqs.message import Message

   
""" Criando conecção com a região west dos EUA """
def getConnection():
    return boto.sqs.connect_to_region(
        "us-west-2", 
        aws_access_key_id=ACCESS_KEY, 
        aws_secret_access_key=SECRET_ACCESS_KEY)


""" Retornando Mensagem na fila da Amazon enviada pela 14Store """
def getOf14Store():
    connection = getConnection()
    queue =  connection.get_queue("Instantiation")
    msgs = connection.receive_message(queue, number_messages=1, message_attributes='All')
    if len(msgs) > 0:
        return msgs[0]
    else:
        return None


""" Enviando uma mensagem de requisicao da 14Store para o 14Factory """
def sendTo14Factory(productName_instance, app_id, node_notification_url, development):

    connection = getConnection()
    queue =  connection.get_queue("Instantiation")

    request = Message()
    request.set_body("instance")
    request.message_attributes = {
        "name" : {
            "data_type": "String",
            "string_value": productName_instance},
        "application_id" : {
            "data_type" : "Number",
            "string_value" : app_id},
        "notification_url" : {
            "data_type" : "String",
            "string_value" : node_notification_url},
        "knife_configuration" : {
            "data_type": "String",
            "string_value": development},
        }
    queue.write(request)


""" Imprimindo uma mensagem da fila e seus atributos """
def print14StoreMessange(message):
    if message is not None:
        print message.get_body()
        print "Nome: ", message.message_attributes["name"]['string_value']
        print "ID da Aplicacao: ", message.message_attributes["application_id"]['string_value']
        print "URL de Notificacao: ", message.message_attributes["notification_url"]['string_value']
        print "Configuracao do Knife: ", message.message_attributes["knife_configuration"]['string_value']
    else:
        print "Não foram encontradas Mensagens"


def listAllMessagesOfQueue(queue):
    # Requerendo mensagem
    connection = getConnection()
    queue =  connection.get_queue(queue)
    messages = connection.receive_message(queue, number_messages=10, message_attributes='All', visibility_timeout=1)

    # Impressão das mensagens
    print "\nA fila tem ao todo: ", queue.count()
    print "\nNúmero de mensagens liberadas: ", len(messages)
    for message in messages:
        print "\n"
        print14StoreMessange(message)

def response14StoreRequest():
    connection = getConnection()
    queue =  connection.get_queue("Instantiation")
    msgs = connection.receive_message(queue, number_messages=1, message_attributes='All')
    if len(msgs) > 0:
        message = msgs[0]
        print14StoreMessange(message)
        queue.delete_message(message)


def listAllQueues():
    connection = getConnection()
    queues = connection.get_all_queues()
    for queue in queues:
        atributos = queue.get_attributes(attributes='All')
        print str(queue.name), " :"
        print "\t Approximate Number Of Messages Not Visible: ",  atributos['ApproximateNumberOfMessagesNotVisible']
        print "\t Message Retention Period: ",  atributos['MessageRetentionPeriod']
        print "\t Approximate Number Of Messages Delayed: ",  atributos['ApproximateNumberOfMessagesDelayed']
        print "\t Maximum Message Size: ",  atributos['MaximumMessageSize'], " B"
        print "\t Created Timestamp",  atributos['CreatedTimestamp']
        print "\t Approximate Number Of Messages: ",  atributos['ApproximateNumberOfMessages']
        print "\t Receive Message Wait Time Seconds: ",  atributos['ReceiveMessageWaitTimeSeconds'], " s"
        print "\t Delay Seconds: ",  atributos['DelaySeconds'], " s"
        print "\t Visibility Timeout: ",  atributos['VisibilityTimeout']
        print "\t Last Modified Timestamp: ",  atributos['LastModifiedTimestamp']
        print "\t ARN: ",  atributos['QueueArn'], "\n"




###############################################################################
#                            Interface do Console                             #
###############################################################################
import os

connection = getConnection()

def execute(option):
    try:

        if option == "1":
            """ Não é possível criar uma nova fila com o mesmo nome de uma já excluida em um tempo mínimo de 60s.
            """
            instantiationQueue = connection.create_queue("Instantiation", 0)
            updateQueue = connection.create_queue("Update", 0)
            print "Duas filas criadas: \n\nInstantiation\n\nUpdate"
            raw_input("\nPressione Enter para voltar ao menu.")

        elif option == "2":
            listAllQueues()
            raw_input("\nPressione Enter para voltar ao menu.")

        elif option == "3":
            produto = raw_input("\tNome do Produto: ")
            id = raw_input("\tID do Produto: ")
            url = raw_input("\tURL de Notificação: ")
            dev = raw_input("\tDesenvolvimento: ")
            sendTo14Factory(produto, int(id), url, dev)
            print "\nSua requisição foi enviada para o SQS com sucesso!"
            raw_input("\nPressione Enter para voltar ao menu.")

        elif option == "4":
            msg = getOf14Store()
            print14StoreMessange(msg)
            raw_input("\nPressione Enter para voltar ao menu.")

        elif option == "5":
            queue = raw_input("Digite o nome da Fila: ")
            listAllMessagesOfQueue(queue)
            raw_input("\nPressione Enter para voltar ao menu.")

        elif option == "6":
            response14StoreRequest()
            raw_input("\nPressione Enter para voltar ao menu.")

    except Exception, e:
        print "\n Exceção: \n"
        print "\t", str(e)
        raw_input("\nPressione Enter para voltar ao menu.")


def main():
    option = "-1"
    while option != "0":
        os.system('clear')

        print "\t 1 - Criar Filas na Amazon"
        print "\t 2 - Retorna Todas as Filas"
        print "\t 3 - Enviar uma requisição para o 14Factory"
        print "\t 4 - Ler uma requisicao da 14Store"
        print "\t 5 - Mostrar as mensagens da fila"
        print "\t 6 - Atender uma requisição da fila"
        print "\t 0 - Sair"
        print "\n Digite sua opção: ",

        option = raw_input()
        os.system('clear')
        execute(option)


if __name__ == "__main__":
    main()